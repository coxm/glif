# GLIF: GL-Inscribed Fonts
GLIF is a utility library for rendering text with OpenGL and FreeType.


## TL;DR
GLIF allows loading of glyph bitmaps and metadata using FreeType. It supports
caching of this data and then packing the bitmaps into OpenGL textures (using
[stb_rectpack](https://github.com/nothings/stb/blob/master/stb_rect_pack.h) for
packing). See the [demos](demo) for examples.

Some basic utilities for decoding UTF8-encoded character strings are included
(e.g. using `glif::utf8_iterator` in the demo above), and packing multiple
different fonts with the same interface is also supported; see
[demo/multiple-fonts.cpp](demo/multiple-fonts.cpp) for an example of this.


## Demos
Several demos are included in the project:

-   [demo/loading.cpp](demo/loading.cpp): a simple example which merely loads
    some glyphs and prints their dimensions.
-   [demo/multiple-fonts.cpp](demo/multiple-fonts.cpp): an example, similar to
    the above, but which loads glyphs from two different font files using the
    same loader.
-   [demo/single-texture.cpp](demo/single-texture.cpp): a more involved example
    which uses a `glif::Loader` to load glyphs and then pack them into a single
    OpenGL texture. This includes some OpenGL boilerplate required to render
    the texture to a quad.
-   [demo/render-sequence.cpp](demo/render-sequence.cpp): the most complex
    demo, but possibly also the most useful. This shows how to load glyphs;
    pack them into a single texture; obtain texture coordinates for individual
    characters and then render a string of them.

Plenty of further examples can be found in [the test suite](test/catch).

### Demo options
In addition to the usual options, please see
[demo/CMakeLists.txt](demo/CMakeLists.txt).


## Dependencies
This library requires at least FreeType, OpenGL and one of
GLEW/[GLAD](https://github.com/Dav1dde/glad). It also includes
[stb_rectpack](https://github.com/nothings/stb/blob/master/stb_rect_pack.h),
which is required if using texture packing. The graphical demos also use SDL2
and GLM, though these are not required to use GLIF in general.


## Building
To build the project, create a build directory and invoke CMake; for example:

    mkdir build
    cd build
    cmake ..
    make

By default all tests and demos are built, and all are memchecked in the test
suite. See [CMakeLists.txt](CMakeLists.txt) for available options. Further
options specific to the demo can be found in
[demo/CMakeLists.txt](demo/CMakeLists.txt).

For example, to disable memcheck testing and change the font paths:

    mkdir build
    cd build
    cmake \
        -DMEMCHECK_TESTS=OFF \
        -DMEMCHECK_DEMOS=OFF \
        -DTEST_FONT_PATH=<fontpath> \
        -DDEMO_FONT_PATH=<fontpath> \
        -DDEMO_ALT_FONT_PATH=<fontpath> \
        ..
    make


## Testing
The test suite is built with [Catch](github.com/philsquared/catch) (the Catch
header is included in this repository). Build the test suite as described
above, then run one of the following from within the build directory.

-   `./test/catch/testmain` to run the unit test binary directly; you can also
    pass options to the Catch2 CLI this way.
-   `ctest` (with `--verbose` for verbose output);
-   `make test`.


## Thanks
Huge thanks to [@rnlf](https://github.com/rnlf) and
[@JoshuaGiles](https://gitlab.com/joshuagiles) for pointers, advice and
debugging assistance. Part of the unicode processing code is taken from
[@rnlf](https://github.com/rnlf)'s
[Motor library](https://github.com/rnlf/motor).


## License
The license (MIT) can be found [here](LICENSE.md). Since part of the unicode
processing code is adapted from [Motor](https://github.com/rnlf/motor),
[its license](LICENSE.motor2d.md) (also MIT) is included.

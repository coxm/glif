/**
 * @file single-texture.cpp
 *
 * Generates a texture from a collection of glyphs. This involces:
 * - loading glyphs from a font file using a @ref glif::Loader;
 * - packing the glyphs into a single GL texture (using stb_rect_pack.h);
 * - rendering a single quad with the texture in a @ref glDrawArrays call.
 */
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "glif/errors.h"
#include "glif/freetype.h"
#include "glif/Loader.h"

#include "./util/ErrorCode.h"
#include "./util/Config.h"
#include "./util/util.h"


util::HandleResult
packGlyphsIntoTexture(util::Config const& config) {
	// Load font and font face.
	std::shared_ptr<FT_LibraryRec_> pFreeType;
	try { pFreeType = glif::freetype::init(); }
	catch (glif::FreeTypeError const& err) {
		std::cerr << "FreeType init failed: " << err.what() << std::endl;
		return {0, util::ERR_FREETYPE_INIT_FAILED};
	}
	std::shared_ptr<FT_FaceRec_> pFontFace;
	try {
		pFontFace = glif::freetype::createFace(
				pFreeType.get(), config.m_pFontPath, config.m_fontIndex);
	}
	catch (glif::FreeTypeError const& err) {
		std::cerr << "Failed to load font face: " << err.what() << std::endl;
		return {0, util::ERR_FREETYPE_FONT_LOAD_FAILED};
	}
	FT_Set_Pixel_Sizes(pFontFace.get(), 0, config.m_fontHeight);

	// The glyphs to load. We'll load all of them at once, then use
	// pack them into a single texture, which will enable us to create a much
	// faster OpenGL program.
	std::cout << "Rendering chars to texture: " << config.m_characters
		<< std::endl;

	// Load the character glyphs and pack into a texture.
	GLuint texture = 0;
	{
		glif::Loader loader;
		loader.load(
			pFontFace.get(),
			glif::Utf8Iterator(std::cbegin(config.m_characters)),
			glif::Utf8Iterator(std::cend(config.m_characters)));
		glActiveTexture(GL_TEXTURE0);
		auto const pack = glif::texture::packTexture2D(
			loader,
			glif::texture::leastTwoPowerDimensions,
			glif::texture::g_defaultTextureSideHint,
			glif::texture::g_defaultMaxTextureSide,
			config.m_padding);
		texture = glif::texture::packGLTexture2D(
			pack.first.data(), pack.second.width, pack.second.height);
		std::cout << "Packed into a texture of size ("
			<< pack.second.width << ", " << pack.second.height << ')'
			<< std::endl;
	}

	return {texture, 0};
}


SDL_Window* g_pWindow = nullptr;
void destroyWindow() {
	if (g_pWindow) {
		SDL_DestroyWindow(g_pWindow);
		g_pWindow = nullptr;
	}
}


SDL_GLContext g_pGLContext = nullptr;
void destroyContext() {
	if (g_pGLContext) {
		SDL_GL_DeleteContext(g_pGLContext);
		g_pGLContext = nullptr;
	}
}


constexpr GLsizei g_numVertices = 6;
constexpr GLsizei g_numComponents = 4; // x, y, s, t.


void
render(GLuint vao) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Technically unnecessary for such a simple example, as the VAO is never
	// unbound.
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, g_numVertices);
	SDL_GL_SwapWindow(g_pWindow);
}


int
main(int const argc, char const* const* argv) {
	// Config.
	util::Config config{argc, argv};
	std::cout << config;

	// SDL.
	// According to the docs, SDL_Quit can be called even if SDL_Init fails.
	atexit(SDL_Quit);
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0) {
		return util::ERR_SDL_INIT_FAILED;
	}

	// Window.
	constexpr Uint32 windowWidth = 640;
	constexpr Uint32 windowHeight = 480;
	g_pWindow = SDL_CreateWindow(
			"An SDL2 window",
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			windowWidth,
			windowHeight,
			SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (g_pWindow == nullptr) {
		return util::ERR_WINDOW_INIT_FAILED;
	}
	atexit(destroyWindow);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(
			SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	// OpenGL context.
	g_pGLContext = SDL_GL_CreateContext(g_pWindow);
	if (!g_pGLContext) {
		return util::ERR_GL_CONTEXT_INIT_FAILED;
	}
	atexit(destroyContext);
	if (auto const error = glGetError(); error != GL_NO_ERROR) {
		std::cerr << "GL error: " << error << std::endl;
		return util::ERR_GL_ERROR_ON_CONTEXT_CREATION;
	}

	// GLEW initialisation.
	glewExperimental = GL_TRUE;
	if (GLenum const glewError = glewInit(); glewError != GLEW_OK) {
		std::cerr << "GLEW error: " << glewGetErrorString(glewError);
		return util::ERR_GLEW_INIT_FAILED;
	}

	// GL configuration.
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	if (HAS_GL_ERROR) {
		return util::ERR_GL_CONFIG_FAILED;
	}

	// Load GL program.
	util::HandleResult program;
	{
		auto const vertShaderSrc{util::loadFile(VERT_SHADER_PATH)};
		auto const fragShaderSrc{util::loadFile(FRAG_SHADER_PATH)};
		program = util::loadProgram(
			vertShaderSrc.c_str(), fragShaderSrc.c_str());
	}
	if (program.error) {
		std::cerr << "GL program error: " << glewGetErrorString(program.error)
			<< std::endl;
		return program.error;
	}

	glUseProgram(program.handle);
	{ // Set the MVP matrix (just use a simple orthographic projection).
		glm::mat4 projection = glm::ortho(
			0.f, float(windowWidth), 0.f, float(windowHeight));
		glUniformMatrix4fv(
			glGetUniformLocation(program.handle, "projection"), 1, GL_FALSE, 
			glm::value_ptr(projection));
	}

	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

	// Initialise VAO.
	GLuint vao = 0;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Send vertex data to a buffer.
	float const xmin = 0.25f * windowWidth;
	float const xmax = 0.75f * windowWidth;
	float const ymin = 0.25f * windowHeight;
	float const ymax = 0.75f * windowHeight;
	float const vertices[g_numComponents * g_numVertices] = {
		xmin, ymax, 0.f, 1.f,
		xmin, ymin, 0.f, 0.f,
		xmax, ymin, 1.f, 0.f,
		xmin, ymax, 0.f, 1.f,
		xmax, ymin, 1.f, 0.f,
		xmax, ymax, 1.f, 1.f
	};
	GLuint vbo = 0;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(
		0, g_numComponents, GL_FLOAT, GL_FALSE,
		g_numComponents * sizeof(float), nullptr);

	// Load glyphs into texture.
	auto const texture = packGlyphsIntoTexture(config);
	if (texture.error) {
		std::cerr << "Error packing texture" << std::endl;
		return texture.error;
	}

	// Main loop.
	bool userQuit = config.m_isHeadless;
	SDL_Event event;
	do {
		render(vao);

		while (SDL_PollEvent(&event) != 0) {
			if (event.type == SDL_QUIT || (
					event.type == SDL_KEYDOWN &&
					event.key.keysym.sym == SDLK_ESCAPE))
			{
				userQuit = true;
				break;
			}
		}
	} while (!userQuit);

	return 0;
}

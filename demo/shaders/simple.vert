#version 330 core
layout (location = 0) in vec4 vertexXYST;

out vec2 st;

uniform mat4 projection;

void main() {
	gl_Position = projection * vec4(vertexXYST.xy, 0.0, 1.0);
	st = vertexXYST.zw;
}

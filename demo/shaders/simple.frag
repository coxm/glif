#version 330 core
in vec2 st;
out vec4 fragColour;

uniform sampler2D glyphs;

void main() {
	// The texture data is all stored in the first, i.e. red, component.
	float alpha = texture(glyphs, st).r;
	fragColour = vec4(0.8, 0.8, 0.8, alpha);
}

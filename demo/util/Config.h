#ifndef GLIF_DEMO__UTIL__CONFIG__H
#define GLIF_DEMO__UTIL__CONFIG__H
#include <ostream>
#include <string>

#include "glif/texture/Padding.h"


namespace util {


struct Config {
	Config(int argc, char const* const* ppArgv);

	Config(Config const&) = delete;
	Config(Config&&) = default;

	Config& operator=(Config const&) = delete;
	Config& operator=(Config&&) = default;

	~Config() noexcept = default;

	std::string m_characters;
	char const* m_pFontPath = DEMO_FONT_PATH;
	char const* m_pAltFontPath = DEMO_ALT_FONT_PATH;
	long m_fontIndex;
	long m_altFontIndex;
	unsigned m_fontHeight;
	unsigned m_altFontHeight;
	unsigned m_windowWidth;
	unsigned m_windowHeight;
	glif::texture::Padding m_padding;
	bool m_isHeadless;
};


template <typename OS>
OS& operator<<(OS& os, Config const& config) {
	return os
		<< "Font path:       " << config.m_pFontPath << std::endl
		<< "Font index:      " << config.m_fontIndex << std::endl
		<< "Font height:     " << config.m_fontHeight << std::endl
		<< "Alt font path:   " << config.m_pFontPath << std::endl
		<< "Alt font index:  " << config.m_fontIndex << std::endl
		<< "Alt font height: " << config.m_fontHeight << std::endl
		<< "Window width:    " << config.m_windowWidth << std::endl
		<< "Window height:   " << config.m_windowHeight << std::endl
		<< "Padding left:    " << int(config.m_padding.left) << std::endl
		<< "Padding right:   " << int(config.m_padding.right) << std::endl
		<< "Padding top:     " << int(config.m_padding.top) << std::endl
		<< "Padding bottom:  " << int(config.m_padding.bottom) << std::endl;
}


} // namespace util
#endif // #ifndef GLIF_DEMO__UTIL__CONFIG__H

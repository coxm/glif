#include <memory>
#include <iostream>

#include <GL/glew.h>
#include <GL/gl.h>

#include "glif/errors.h"
#include "glif/freetype.h"
#include "glif/Loader.h"

#include "./ErrorCode.h"
#include "./packGlyphs.h"


namespace util {


HandleResult
packGlyphsIntoTexture(
	char const* const pFontPath,
	long const fontIndex,
	unsigned fontHeight,
	std::string const& characters,
	glif::texture::Padding padding)
{
	// Load font and font face.
	std::shared_ptr<FT_LibraryRec_> pFreeType;
	try { pFreeType = glif::freetype::init(); }
	catch (glif::FreeTypeError const& err) {
		std::cerr << "FreeType init failed: " << err.what() << std::endl;
		return {0, util::ERR_FREETYPE_INIT_FAILED};
	}
	std::shared_ptr<FT_FaceRec_> pFontFace;
	try {
		pFontFace = glif::freetype::createFace(
				pFreeType.get(), pFontPath, fontIndex);
	}
	catch (glif::FreeTypeError const& err) {
		std::cerr << "Failed to load font face: " << err.what() << std::endl;
		return {0, util::ERR_FREETYPE_FONT_LOAD_FAILED};
	}
	FT_Set_Pixel_Sizes(pFontFace.get(), 0, fontHeight);

	// The glyphs to load. We'll load all of them at once, then use
	// rectpack2D to pack them into a single texture, which will enable us
	// to create a much faster OpenGL program.
	std::cout << "Rendering chars to texture: " << characters << std::endl;

	// Load the character glyphs and pack into a texture.
	glif::Loader loader;
	loader.load(
		pFontFace.get(),
		glif::Utf8Iterator(std::cbegin(characters)),
		glif::Utf8Iterator(std::cend(characters)));
	glActiveTexture(GL_TEXTURE0);
	auto const pack = loader.packGLTexture2D(
		glif::texture::leastTwoPowerDimensions,
		glif::texture::g_defaultMaxTextureSide,
		glif::texture::g_defaultDiscardStep,
		padding);
	std::cout << "Packed into a texture of size (" << pack.first.w << ", "
		<< pack.first.h << ')' << std::endl;
	return {pack.second, 0};
}


} // namespace util

#include <string>

#include "glif/texture/Padding.h"

#include "./util.h"


namespace util {


HandleResult
packGlyphsIntoTexture(
	char const* const pFontPath,
	long const fontIndex,
	unsigned fontHeight,
	std::string const& characters,
	glif::texture::Padding padding);


} // namespace util

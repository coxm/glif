#include <fstream>
#include <sstream>
#include <iostream>

#include "./util.h"
#include "./ErrorCode.h"


namespace util {


std::string
getLog(GLuint id, log_length_getter getLogLength, log_getter getLogContent) {
	GLint length = 0;
	getLogLength(id, GL_INFO_LOG_LENGTH, &length);
	std::string log(length, '\0');
	if (length == 0) {
		return log;
	}
	getLogContent(id, length, nullptr, &log[0]);
	return log;
}


bool
checkGLErrors(char const* const pFile, unsigned long line) {
	bool foundErrors = false;
	GLenum error;
	while ((error = glGetError())) {
		std::cerr << "GL error [" << pFile << ':' << line << "] "
			<< glewGetErrorString(error) << std::endl;
		foundErrors = true;
	}
	return foundErrors;
}


std::string
loadFile(char const* const pFilePath) {
	std::ifstream ifs{pFilePath};
	if (!ifs.good()) {
		throw std::runtime_error{std::string("Can't load file: ") + pFilePath};
	}
	std::stringstream buffer;
	buffer << ifs.rdbuf();
	return buffer.str();
}


HandleResult
compileShader(GLenum type, std::string const& source) {
	auto shader = glCreateShader(type);
	{
		auto const* const pSrc =
			reinterpret_cast<GLchar const*>(source.c_str());
		auto const len = GLint(source.size());
		glShaderSource(shader, 1, &pSrc, &len);
	}
	glCompileShader(shader);
	GLint compiled = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (auto const log{getLog(shader, glGetShaderiv, glGetShaderInfoLog)};
		!log.empty())
	{
		std::cout << "---- Shader log ----\n" << log
			<< "\n---- End of log ----" << std::endl;
	}
	if (HAS_GL_ERROR) {
		glDeleteShader(shader);
		return {0, ERR_GL_SHADER_COMPILATION_FAILED};
	}
	return {shader, 0};
}


HandleResult
loadProgram(
	char const* const pVertShaderSrc,
	char const* const pFragShaderSrc)
{
	// Load vertex shader.
	auto const vertShader = compileShader(GL_VERTEX_SHADER, pVertShaderSrc);
	if (vertShader.error) {
		std::cerr << "Failed to compile vertex shader!" << std::endl;
		glDeleteShader(vertShader.handle);
		return {0, vertShader.error};
	}

	// Load fragment shader.
	auto const fragShader = compileShader(GL_FRAGMENT_SHADER, pFragShaderSrc);
	if (fragShader.error) {
		std::cerr << "Failed to compile fragment shader!" << std::endl;
		glDeleteShader(vertShader.handle);
		return {0, fragShader.error};
	}

	// Create program. Attach shaders and mark for deletion.
	auto const program = glCreateProgram();
	glAttachShader(program, vertShader.handle);
	glAttachShader(program, fragShader.handle);
	glDeleteShader(vertShader.handle);
	glDeleteShader(fragShader.handle);
	if (HAS_GL_ERROR) {
		glDeleteProgram(program);
		return {0, ERR_GL_PROGRAM_CREATION_FAILED};
	}

	// Link program.
	glLinkProgram(program);
	if (auto const log{getLog(program, glGetProgramiv, glGetProgramInfoLog)};
			!log.empty())
	{
		std::cout << "---- Program log ----\n" << log <<
			"\n---- End of log ----" << std::endl;
	}
	GLint linked = GL_FALSE;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (linked != GL_TRUE || HAS_GL_ERROR) {
		std::cerr << "Failed to link program!" << std::endl;
		glDeleteProgram(program);
		return {0, ERR_GL_PROGRAM_LINK_FAILED};
	}

	return {program, 0};
}


} // namespace util

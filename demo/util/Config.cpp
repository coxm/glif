#include <cstdlib>
#include <string>
#include <iostream>
#include <limits>

#include "./Config.h"


namespace util {


inline unsigned
stou(char const* const pInput) {
	auto const i = std::stoul(pInput);
	if (std::numeric_limits<unsigned>::min() <= i &&
		std::numeric_limits<unsigned>::max() >= i)
	{
		return i;
	}
	throw std::out_of_range(pInput);
}


inline std::int8_t
stoi8(char const* const pInput) {
	auto const i = std::stoi(pInput);
	if (std::numeric_limits<std::int8_t>::min() <= i &&
		std::numeric_limits<std::int8_t>::max() >= i)
	{
		return i;
	}
	throw std::out_of_range(pInput);
}


template <typename ParseFn>
using parse_result_t = std::result_of_t<ParseFn(char const*)>;


template <typename ParseFn>
parse_result_t<ParseFn>
parseInt(
	char const* const pVarName,
	ParseFn parse,
	parse_result_t<ParseFn> defaultValue)
{
	char const* const pEnvVar = std::getenv(pVarName);
	if (pEnvVar) {
		try {
			return parse(pEnvVar);
		}
		catch (std::invalid_argument const& err) {
			std::cerr << "Config error: invalid value (" << pVarName << ')'
				<< std::endl;
		}
		catch (std::out_of_range const& err) {
			std::cerr << "Config error: out of range (" << pVarName << ')'
				<< std::endl;
		}
	}

	return defaultValue;
}


Config::Config(int const argc, char const* const* const ppArgv)
	:	m_characters{}
	,	m_fontIndex{parseInt("FONT_INDEX", stou, DEMO_FONT_INDEX)}
	,	m_altFontIndex{parseInt("ALT_FONT_INDEX", stou, DEMO_ALT_FONT_INDEX)}
	,	m_fontHeight{parseInt("FONT_HEIGHT", stou, DEMO_FONT_HEIGHT)}
	,	m_altFontHeight{
			parseInt("ALT_FONT_HEIGHT", stou, DEMO_ALT_FONT_HEIGHT)}
	,	m_windowWidth{parseInt("WINDOW_WIDTH", stou, 800u)}
	,	m_windowHeight{parseInt("WINDOW_HEIGHT", stou, 600u)}
	,	m_padding{glif::texture::Padding::none()}
	,	m_isHeadless{argc > 1 && std::string("--headless") == ppArgv[1]}
{
	if (char const* const pFontPath = std::getenv("FONT_PATH")) {
		m_pFontPath = pFontPath;
	}
	if (char const* const pAltFontPath = std::getenv("ALT_FONT_PATH")) {
		m_pAltFontPath = pAltFontPath;
	}
	if (char const* const pCharacters = std::getenv("CHARACTERS")) {
		m_characters = pCharacters;
	}
	else {
		m_characters = 
			" !\"#$%&\'()*+,-./0123456789:;<=>?@"
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`"
			"abcdefghijklmnopqrstuvwxyz{|}~";
	}

	auto const paddingAll = parseInt("PADDING", stoi8, std::int8_t(1));
	m_padding.top = parseInt("PADDING_TOP", stoi8, paddingAll);
	m_padding.bottom = parseInt("PADDING_BOTTOM", stoi8, paddingAll);
	m_padding.left = parseInt("PADDING_LEFT", stoi8, paddingAll);
	m_padding.right = parseInt("PADDING_RIGHT", stoi8, paddingAll);
}


} // namespace util

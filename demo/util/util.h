#ifndef GLIF_DEMO__UTIL__UTIL__H
#define GLIF_DEMO__UTIL__UTIL__H
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>


namespace util {


struct HandleResult {
	GLuint handle;
	int error;
};


/** A function which can retrieve the length of a program/shader log. */
typedef void (*log_length_getter)(GLuint id, GLenum what, GLint* pLength);


/** A function which can retrieve a program/shader log. */
typedef void (*log_getter)(
		GLuint id, GLint length, GLsizei* pLength, GLchar* pInfoLog);


std::string
getLog(GLuint id, log_length_getter getLogLength, log_getter getLogContent);


bool
checkGLErrors(char const* const pFile, unsigned long line);


std::string
loadFile(char const* const pFilePath);


HandleResult
compileShader(GLenum type, std::string const& source);


HandleResult
loadProgram(
	char const* const pVertShaderSrc,
	char const* const pFragShaderSrc);


} // namespace util


#define HAS_GL_ERROR util::checkGLErrors(__FILE__, __LINE__)


#endif // #ifndef GLIF_DEMO__UTIL__UTIL__H

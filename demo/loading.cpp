/**
 * @file laoding.cpp
 *
 * A simple demonstration of how to load glyphs with a @ref glif::Loader.
 */
#include <memory>
#include <string>
#include <iostream>

#include "glif/unicode.h"
#include "glif/Loader.h"

#include "./util/ErrorCode.h"
#include "./util/Config.h"


int
main(int const argc, char const* const* const ppArgv) {
	util::Config const config(argc, ppArgv);
	std::cout << config;

	// Initialise FreeType.
	std::shared_ptr<FT_LibraryRec_> pFreeType;
	try { pFreeType = glif::freetype::init(); }
	catch (glif::FreeTypeError const& err) {
		std::cerr << "FreeType init failed: " << err.what() << std::endl;
		return util::ERR_FREETYPE_INIT_FAILED;
	}

	// Load font face.
	std::shared_ptr<FT_FaceRec_> pFontFace;
	try {
		pFontFace = glif::freetype::createFace(
			pFreeType.get(), config.m_pFontPath, config.m_fontIndex);
	}
	catch (glif::FreeTypeError const& err) {
		std::cerr << "Failed to load font face: " << err.what() << std::endl;
		return util::ERR_FREETYPE_FONT_LOAD_FAILED;
	}
	FT_Set_Pixel_Sizes(pFontFace.get(), 0, config.m_fontHeight);

	// Create a glif::Loader for storing glyph data.
	glif::Loader loader;

	// Load the 'a' glyph.
	loader.load(pFontFace.get(), 'a');

	// Load the 'α' glyph.
	glif::unicode_codepoint const alpha = 945;
	loader.load(pFontFace.get(), alpha);

	// Load an entire UTF8-encoded string.
	loader.load(
		pFontFace.get(),
		glif::utf8_iterator(std::cbegin(config.m_characters)),
		glif::utf8_iterator(std::cend(config.m_characters)));

	// Print some details about the loaded glyphs.
	for (auto const& pair: loader.glyphs()) {
		std::cout << "Glyph " << pair.first << ":" << std::endl
			<< "  width:  " << pair.second.width << std::endl
			<< "  height: " << pair.second.height << std::endl;
	}

	return EXIT_SUCCESS;
}

/**
 * @file multiple-fonts.cpp
 *
 * A simple demonstration of how to load multiple fonts with a single @ref
 * glif::Loader.
 */
#include <memory>
#include <string>
#include <iostream>

#include "glif/unicode.h"
#include "glif/Loader.h"

#include "./util/ErrorCode.h"
#include "./util/Config.h"


/** Custom key type for storing multiple fonts in a @ref glif::Loader. */
struct CustomKey {
	constexpr static unsigned faceOne = 1u;
	constexpr static unsigned faceTwo = 2u;

	inline bool
	operator<(CustomKey other) const noexcept {
		return fontFaceId < other.fontFaceId || (
			fontFaceId == other.fontFaceId && codepoint < other.codepoint);
	}

	glif::unicode_codepoint codepoint;
	unsigned fontFaceId;
};


/** Custom traits class for @ref CustomKey. */
struct CustomKeyTraits {
	using key_type = CustomKey;

	/** Retrieve a codepoint from a @ref CustomKey. */
	static glif::unicode_codepoint
	codepoint(key_type const& key)
	{ return key.codepoint; }
};


/**
 * The loader type.
 *
 * We've used a custom key type, with a custom key traits type for deducing the
 * codepoint from a key.
 */
using MultiFontLoader = glif::Loader<CustomKey, glif::Glyph, CustomKeyTraits>;


int
main(int const argc, char const* const* const ppArgv) {
	util::Config const config(argc, ppArgv);
	std::cout << config;

	// Initialise FreeType.
	std::shared_ptr<FT_LibraryRec_> pFreeType;
	try { pFreeType = glif::freetype::init(); }
	catch (glif::FreeTypeError const& err) {
		std::cerr << "FreeType init failed: " << err.what() << std::endl;
		return util::ERR_FREETYPE_INIT_FAILED;
	}

	// Load font faces.
	std::shared_ptr<FT_FaceRec_> pFontFaceOne;
	std::shared_ptr<FT_FaceRec_> pFontFaceTwo;
	try {
		pFontFaceOne = glif::freetype::createFace(
			pFreeType.get(), config.m_pFontPath, config.m_fontIndex);
		pFontFaceTwo = glif::freetype::createFace(
			pFreeType.get(), config.m_pAltFontPath, config.m_altFontIndex);
	}
	catch (glif::FreeTypeError const& err) {
		std::cerr << "Failed to load font face: " << err.what() << std::endl;
		return util::ERR_FREETYPE_FONT_LOAD_FAILED;
	}
	FT_Set_Pixel_Sizes(pFontFaceOne.get(), 0, config.m_fontHeight);
	FT_Set_Pixel_Sizes(pFontFaceTwo.get(), 0, config.m_altFontHeight);

	// Create a glif::Loader for storing glyph data from multiple fonts.
	MultiFontLoader loader;

	// Load an entire UTF8-encoded string.
	{
		auto const beg = glif::utf8_iterator(std::cbegin(config.m_characters));
		auto const end = glif::utf8_iterator(std::cend(config.m_characters));
		for (auto it = beg; it != end; ++it) {
			loader.load(pFontFaceOne.get(), {*it, CustomKey::faceOne});
		}
		for (auto it = beg; it != end; ++it) {
			loader.load(pFontFaceTwo.get(), {*it, CustomKey::faceTwo});
		}
	}

	// Print some details about the loaded glyphs.
	for (auto const& pair: loader.glyphs()) {
		std::cout << "(Font " << pair.first.fontFaceId
			<< ", codepoint " << pair.first.codepoint << "):" << std::endl
			<< "  width:  " << pair.second.width << std::endl
			<< "  height: " << pair.second.height << std::endl;
	}

	return EXIT_SUCCESS;
}

#ifndef GLIF_INCLUDE__GLIF__ERRORS__H
#define GLIF_INCLUDE__GLIF__ERRORS__H
#include <stdexcept>


namespace glif {


namespace freetype { char const* getErrorString(int) noexcept; }


/** Base exception for errors (intentionally) thrown by this library. */
class GlifError
	:	public std::runtime_error
{
public:
	using std::runtime_error::runtime_error;
};


class InvalidUtf8InitialByte
	:	GlifError
{
public:
	inline InvalidUtf8InitialByte(std::string const& msg, std::uint8_t byte_)
		:	GlifError(msg)
		,	byte(byte_)
	{
	}

	inline InvalidUtf8InitialByte(std::uint8_t byte_)
		:	InvalidUtf8InitialByte("UTF8", byte_)
	{
	}

	std::uint8_t const byte;
};


/** Exception thrown when encountering a FreeType error. */
class FreeTypeError
	:	public GlifError
{
public:
	inline FreeTypeError(int ftErrorCode)
		:	GlifError(
				std::string("glif::FreeTypeError: ") +
				freetype::getErrorString(ftErrorCode))
		,	m_ftErrorCode(ftErrorCode)
	{
	}

	inline int ftErrorCode() const noexcept { return m_ftErrorCode; }

private:
	int m_ftErrorCode;
};


/** Exception thrown when packing a texture failed. */
class PackingFailed
	:	public GlifError
{
public:
	using GlifError::GlifError;
};


/** Exception thrown when a requested texture is too large. */
class TextureTooLarge
	:	public GlifError
{
public:
	using GlifError::GlifError;
};


} // namespace glif
#endif // #ifndef GLIF_INCLUDE__GLIF__ERRORS__H

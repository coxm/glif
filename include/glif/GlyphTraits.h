#ifndef GLIF_INCLUDE__GLIF__HEADERTRAITS__H
#define GLIF_INCLUDE__GLIF__HEADERTRAITS__H
#include <utility>

#include "./Dimensions.h"


namespace glif {


struct UVBounds {
	float xmin;
	float xmax;
	float ymin;
	float ymax;
};


template <typename GlyphT>
struct GlyphTraits {
	using glyph_type = GlyphT;
	using value_type = glyph_type;
	using reference = value_type&;
	using const_reference = value_type const&;

#define GLIF_ADD_ACCESS(method, member) \
	inline static auto& \
	method(reference glyph) noexcept \
	{ return glyph.member; } \
	inline static auto const& \
	method(const_reference glyph) noexcept \
	{ return glyph.member; }

	GLIF_ADD_ACCESS(textureX, textureX)
	GLIF_ADD_ACCESS(textureY, textureY)
	GLIF_ADD_ACCESS(width, width)
	GLIF_ADD_ACCESS(height, height)
#undef GLIF_ADD_ACCESS

	inline static UVBounds
	uvBounds(const_reference glyph, Dimensions dimensions) noexcept {
		auto const x = textureX(glyph);
		auto const y = textureY(glyph);
		auto const w = width(glyph);
		auto const h = height(glyph);
		return {
			float(x) / dimensions.width,
			float(x + w) / dimensions.width,
			float(y) / dimensions.height,
			float(y + h) / dimensions.height,
		};
	}
};


/**
 * GlyphTraits specialisation for pairs.
 *
 * Enables glyph traits to be used with nodes from associative containers.
 */
template <typename Key, typename GlyphT>
struct GlyphTraits<std::pair<Key, GlyphT>> {
	using glyph_type = GlyphT;
	using value_type = std::pair<Key, glyph_type>;
	using reference = value_type&;
	using const_reference = value_type const&;
	using base_traits = GlyphTraits<glyph_type>;

#define GLIF_ADD_PAIR_VALUE_ACCESS(method) \
	inline static auto& \
	method(reference pair) noexcept \
	{ return base_traits::method(pair.second); } \
	inline static auto const& \
	method(const_reference pair) noexcept \
	{ return base_traits::method(pair.second); } \

	GLIF_ADD_PAIR_VALUE_ACCESS(textureX)
	GLIF_ADD_PAIR_VALUE_ACCESS(textureY)
	GLIF_ADD_PAIR_VALUE_ACCESS(width)
	GLIF_ADD_PAIR_VALUE_ACCESS(height)
#undef GLIF_ADD_PAIR_VALUE_ACCESS

	inline static UVBounds
	uvBounds(const_reference glyph, Dimensions dimensions) noexcept {
		return base_traits::uvBounds(glyph, dimensions);
	}
};


/** Special glyph traits for glyphs stored in a Loader which caches bitmaps. */
template <typename LoaderT>
struct LoaderGlyphTraits {
	using loader_type = LoaderT;
	using base_traits = GlyphTraits<typename LoaderT::value_type>;
	using glyph_type = typename base_traits::glyph_type;
	using value_type = typename base_traits::value_type;
	using reference = typename base_traits::reference;
	using const_reference = typename base_traits::const_reference;

	LoaderGlyphTraits(loader_type* pLoader)
		:	loader{pLoader}
	{
	}


#define GLIF_ADD_BASE_ACCESS(method) \
	inline static auto& \
	method(reference glyph) noexcept \
	{ return base_traits::method(glyph); } \
	inline static auto const& \
	method(const_reference glyph) noexcept \
	{ return base_traits::method(glyph); } \

	GLIF_ADD_BASE_ACCESS(textureX)
	GLIF_ADD_BASE_ACCESS(textureY)
	GLIF_ADD_BASE_ACCESS(width)
	GLIF_ADD_BASE_ACCESS(height)
#undef GLIF_ADD_BASE_ACCESS

	inline static UVBounds
	uvBounds(const_reference glyph, Dimensions dimensions) noexcept {
		return base_traits::uvBounds(glyph, dimensions);
	}

	inline decltype(auto)
	bitmapBeginOf(const_reference glyph)
	{ return loader->bitmapBeginOf(glyph); }

	inline decltype(auto)
	bitmapEndOf(const_reference glyph)
	{ return loader->bitmapEndOf(glyph); }

	loader_type* loader;
};


template <typename LoaderT>
inline LoaderGlyphTraits<LoaderT>
loaderTraits(LoaderT* loader) noexcept {
	return LoaderGlyphTraits<LoaderT>(loader);
}


} // namespace glif
#endif // #ifndef GLIF_INCLUDE__GLIF__HEADERTRAITS__H

#include <algorithm>


namespace glif {
namespace texture {


template <typename GlyphTraitsT, typename GlyphInputIter, typename OutputIter>
void
copyTexture2D(
	GlyphTraitsT traits,
	GlyphInputIter begin,
	GlyphInputIter const end,
	OutputIter iTexOut,
	unsigned textureWidth)
{
	for (; begin != end; ++begin){
		auto const x = traits.textureX(*begin);
		auto const y = traits.textureY(*begin);
		auto const width = traits.width(*begin);
		auto const height = traits.height(*begin);
		auto const iBitmapEnd = traits.bitmapEndOf(*begin);
		for (int row = height - 1; row >= 0; --row) {
			auto const* const pSrcBegin = iBitmapEnd - (1 + row) * width;
			auto const* const pSrcEnd = pSrcBegin + width;
			std::copy(
				pSrcBegin, pSrcEnd, iTexOut + x + (y + row) * textureWidth);
		}
	}
}


} // namespace texture
} // namespace glif

#ifndef GLIF_INCLUDE__GLIF__TEXTURE__PACK__H
#define GLIF_INCLUDE__GLIF__TEXTURE__PACK__H
#include <cstdint>
#include <memory>
#include <vector>


#include <stb_rect_pack.h>

#include "../gl.h"
#include "../GlyphTraits.h"
#include "../errors.h"
#include "../Dimensions.h"
#include "./copy.h"
#include "./Padding.h"


namespace glif {
namespace texture {


std::uint32_t
leastTwoPowerGte(std::uint32_t n) noexcept;


inline Dimensions
leastTwoPowerDimensions(Dimensions const dim) {
	auto const pot = leastTwoPowerGte(std::max(dim.width, dim.height));
	return {int(pot), int(pot)};
}


constexpr int g_defaultMaxTextureSide = 0;
constexpr std::uint32_t g_defaultTextureSideHint = 256;


Dimensions
attemptRectPack(
	stbrp_rect* pRectangles,
	int numRectangles,
	std::uint32_t textureSideHint = g_defaultTextureSideHint,
	int maxTextureSide = g_defaultMaxTextureSide,
	Padding const padding = Padding::none()
);


using DimensionsModifier = Dimensions(Dimensions dim);


using bitmap_vector = std::vector<std::uint8_t>;


/**
 * Pack glyphs into a texture.
 *
 * @tparam GlyphForwardIter a forward iterator type for glyph metadata.
 *
 * @param traits a traits class instance for manipulating glyphs.
 * @param begin begin iterator to the glyph metadata collection.
 * @param end one-past-the-end iterator to the glyph metadata collection.
 * @param adjustDimensions a function which will adjust the output texture bin
 * size accordingly, for example by using the least possible power of two for
 * width and height.
 *
 * @returns a pair consisting of the raw bitmap data and the composite
 * texture's dimensions.
 */
template <typename GlyphTraitsT, typename GlyphFwdIter>
std::pair<std::vector<std::uint8_t>, Dimensions>
packTexture2D(
	GlyphTraitsT traits,
	GlyphFwdIter begin,
	GlyphFwdIter end,
	DimensionsModifier adjustDimensions = leastTwoPowerDimensions,
	std::uint32_t textureSideHint = g_defaultTextureSideHint,
	int const maxTextureSide = g_defaultMaxTextureSide,
	Padding padding = Padding::none()
) {
	auto const numRects = int(std::distance(begin, end));
	std::vector<stbrp_rect> rectangles{std::size_t(numRects), stbrp_rect{
		0,
		stbrp_coord(0), stbrp_coord(0),
		stbrp_coord(0), stbrp_coord(0),
		0 // was_packed: false.
	}};
	auto* const pRects = rectangles.data();
	auto const padX = stbrp_coord(padding.top + padding.bottom);
	auto const padY = stbrp_coord(padding.left + padding.right);
	int id = 0;
	for (auto it = begin; it != end; ++it, ++id) {
		pRects[id].id = id;
		pRects[id].w = stbrp_coord(traits.width(*it)) + padX;
		pRects[id].h = stbrp_coord(traits.height(*it)) + padY;
	}
	auto texSize = attemptRectPack(
		pRects, numRects, textureSideHint, maxTextureSide, padding);
	texSize = adjustDimensions(texSize);

	// Copy texture coordinates back to the glyphs.
	unsigned index = 0;
	for (auto it = begin; it != end; ++it, ++index) {
		auto const& rect = rectangles[index];
		traits.textureX(*it) = rect.x + padding.left;
		traits.textureY(*it) = rect.y + padding.bottom;
	}

	std::pair<bitmap_vector, Dimensions> result{
		texSize.width * texSize.height,
		texSize
	};
	copyTexture2D(traits, begin, end, result.first.data(), texSize.width);
	return result;
}


/**
 * Pack glyphs from a loader into a texture.
 *
 * Similar to @ref packTexture2D above, only most arguments are deduced from
 * the loader.
 */
template <typename LoaderT>
inline std::pair<bitmap_vector, Dimensions>
packTexture2D(
	LoaderT& loader,
	DimensionsModifier adjustDimensions = leastTwoPowerDimensions,
	std::uint32_t textureSideHint = g_defaultTextureSideHint,
	int maxTextureSide = g_defaultMaxTextureSide,
	Padding padding = Padding::none())
{
	return packTexture2D(
		LoaderGlyphTraits<LoaderT>(&loader),
		std::begin(loader.glyphs()),
		std::end(loader.glyphs()),
		adjustDimensions,
		textureSideHint,
		maxTextureSide,
		padding);
}


GLuint
packGLTexture2D(std::uint8_t const* pTextureData, int width, int height);


/**
 * Pack glyphs from a loader into an OpenGL texture.
 *
 * Similar to @ref packTexture2D, only the bitmaps are packed into an OpenGL
 * texture and the texture handle returned.
 *
 * @returns a pair containing the texture dimensions and handle.
 */
template <typename ...Args>
std::pair<Dimensions, GLuint>
packGLTexture2D(Args&&... args) {
	auto const pack{packTexture2D(std::forward<Args>(args)...)};
	auto const texture = packGLTexture2D(
		pack.first.data(), pack.second.width, pack.second.height);
	return {pack.second, texture};
}


} // namespace texture
} // namespace glif
#endif // #ifndef GLIF_INCLUDE__GLIF__TEXTURE__PACK__H

#ifndef GLIF_INCLUDE__GLIF__TEXTURE__PADDING__H
#define GLIF_INCLUDE__GLIF__TEXTURE__PADDING__H
#include <cstdint>


namespace glif {
namespace texture {


struct Padding {
	inline static constexpr Padding none() { return Padding(0); }

	inline constexpr Padding(
		std::int8_t top_,
		std::int8_t bottom_,
		std::int8_t left_,
		std::int8_t right_
	)
		:	top(top_)
		,	bottom(bottom_)
		,	left(left_)
		,	right(right_)
	{
	}

	inline constexpr Padding(std::int8_t all)
		:	Padding(all, all, all, all)
	{
	}

	template <typename Rect>
	inline Rect apply(Rect rect) const noexcept {
		rect.h += top + bottom;
		rect.w += left + right;
		return rect;
	}

	std::int8_t top;
	std::int8_t bottom;
	std::int8_t left;
	std::int8_t right;
};


} // namespace texture
} // namespace glif
#endif // #ifndef GLIF_INCLUDE__GLIF__TEXTURE__PADDING__H

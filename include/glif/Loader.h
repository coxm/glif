#ifndef GLIF_INCLUDE__GLIF__LOADER__H
#define GLIF_INCLUDE__GLIF__LOADER__H
#include <iterator>
#include <cstddef>
#include <cstdint>
#include <vector>
#include <map>

#include "./freetype.h"
#include "./unicode.h"
#include "./texture/pack.h"
#include "./Glyph.h"
#include "./KeyTraits.h"


namespace glif {


template <
	typename Key = unicode_codepoint,
	typename GlyphT = Glyph,
	typename KeyTraitsT = KeyTraits<Key>>
class Loader {
public:
	using byte_type = std::uint8_t;
	using key_type = Key;
	using key_traits = KeyTraitsT;
	using glyph_type = GlyphT;
	using glyph_container_type = std::map<key_type, glyph_type>;
	using glyph_iterator = typename glyph_container_type::iterator;
	using const_glyph_iterator =
		typename glyph_container_type::const_iterator;
	using value_type = typename glyph_container_type::value_type;

	inline Loader()
		:	m_glyphs()
		,	m_bitmapData()
	{
	}

	inline glyph_container_type&
	glyphs() noexcept
	{ return m_glyphs; }

	inline glyph_container_type const&
	glyphs() const noexcept
	{ return m_glyphs; }

	std::vector<byte_type>
	bitmaps() noexcept
	{ return m_bitmapData; }

	std::vector<byte_type> const
	bitmaps() const noexcept
	{ return m_bitmapData; }

	/** Find a glyph. */
	inline glyph_iterator
	findGlyph(key_type key)
	{ return m_glyphs.find(key); }

	/** Find a glyph. */
	inline const_glyph_iterator
	findGlyph(key_type key) const
	{ return m_glyphs.find(key); }

	inline byte_type const*
	bitmapBeginOf(glyph_type const& glyph) const
	{ return &m_bitmapData[glyph.begin]; }

	inline byte_type const*
	bitmapBeginOf(value_type const& node) const
	{ return bitmapBeginOf(node.second); }

	inline byte_type const*
	bitmapEndOf(glyph_type const& glyph) const
	{ return &m_bitmapData[glyph.end]; }

	inline byte_type const*
	bitmapEndOf(value_type const& node) const
	{ return bitmapEndOf(node.second); }

	/**
	 * Load glyph data for a codepoint.
	 *
	 * If the codepoint has not been loaded, it will be loaded using the font
	 * face provided.
	 *
	 * @param pFace the font face.
	 * @param key the key to store this glyph under.
	 *
	 * @returns a pair consisting of an iterator to the element, and a bool
	 * indicating whether the element was newly inserted (true) or already
	 * existed (false).
	 *
	 * @note provides a strong exception guarantee.
	 */
	std::pair<const_glyph_iterator, bool>
	load(FT_Face pFace, key_type key) {
		auto const it = m_glyphs.find(key);
		if (it != m_glyphs.end()) {
			return {it, false};
		}

		freetype::loadGlyphIntoSlot(pFace, key_traits::codepoint(key));
		auto const* const pGlyph = pFace->glyph;
		auto const& bitmap{pGlyph->bitmap};
		std::size_t const glyphBytes{bitmap.width * bitmap.rows};
		std::size_t const glyphBegin{m_bitmapData.size()};
		std::size_t const glyphEnd{glyphBegin + glyphBytes};
		m_bitmapData.reserve(glyphEnd);

		auto const emplacement = m_glyphs.emplace(
			key, GlyphT(pGlyph, glyphBegin, glyphEnd));

		std::copy(bitmap.buffer, bitmap.buffer + glyphBytes,
		          std::back_inserter(m_bitmapData));
		return emplacement;
	}

	template <typename KeyInputIter>
	unsigned
	load(FT_Face pFace, KeyInputIter begin, KeyInputIter end) {
		unsigned count = 0u;
		for (; begin != end; ++begin, ++count) {
			load(pFace, *begin);
		}
		return count;
	}

private:
	glyph_container_type m_glyphs;
	std::vector<byte_type> m_bitmapData;
};


} // namespace glif
#endif // #ifndef GLIF_INCLUDE__GLIF__LOADER__H

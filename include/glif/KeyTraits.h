#include <cstdint>


namespace glif {


/**
 * Traits class for different @ref Loader key types.
 *
 * Allows the @ref Loader to be used with e.g. multiple fonts.
 */
template <typename Key>
struct KeyTraits {};


/** KeyTraits specialisation for keys which consist only of a codepoint. */
template <>
struct KeyTraits<unicode_codepoint> {
	using key_type = std::uint32_t;

	/** Given a character key, retrieve the unicode codepoint. */
	inline constexpr static
	key_type codepoint(key_type key) noexcept {
		return key;
	}
};


} // namespace glif

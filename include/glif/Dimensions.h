#ifndef GLIF_INCLUDE__GLIF__DIMENSIONS__H
#define GLIF_INCLUDE__GLIF__DIMENSIONS__H
namespace glif {


struct Dimensions {
	int width;
	int height;

	inline Dimensions(int w, int h)
		: width(w)
		, height(h)
	{
	}
};


} // namespace glif
#endif // #ifndef GLIF_INCLUDE__GLIF__DIMENSIONS__H

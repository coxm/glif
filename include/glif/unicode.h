#ifndef GLIF_INCLUDE__GLIF__UNICODE__H
#define GLIF_INCLUDE__GLIF__UNICODE__H
#include <cstdint>
#include <utility>
#include <string>
#include <type_traits>
#include <iterator>

#include "glif/errors.h"


namespace glif {


using unicode_codepoint = std::uint32_t;


constexpr unsigned
countNextUtf8CharBytesNoexcept(std::uint8_t const initialByte) {
	if (initialByte < 128u) {
		return 1u;
	}
	if ((initialByte & 0b11100000) == 0b11000000) {
		return 2u;
	}
	if ((initialByte & 0b11110000) == 0b11100000) {
		return 3u;
	}
	if ((initialByte & 0b11111000) == 0b11110000) {
		return 4u;
	}
	return 0u;
}


template <typename Byte>
inline unsigned
countNextUtf8CharBytesNoexcept(Byte const initialByte) {
	static_assert(std::is_pod<Byte>::value && sizeof(Byte) == 1,
	              "Expected byte type");
	return countNextUtf8CharBytesNoexcept(
		*reinterpret_cast<std::uint8_t const*>(&initialByte));
}


inline unsigned
countNextUtf8CharBytes(char const* pBytes) {
	std::uint8_t const initialByte =
		reinterpret_cast<std::uint8_t const*>(pBytes)[0];
	unsigned const count = countNextUtf8CharBytesNoexcept(initialByte);
	if (count == 0u) {
		throw InvalidUtf8InitialByte(pBytes, initialByte);
	}
	return count;
}


template <typename Byte>
inline unsigned
countNextUtf8CharBytes(Byte const initialByte) {
	std::uint8_t const ch =
		*reinterpret_cast<std::uint8_t const*>(&initialByte);
	unsigned const count = countNextUtf8CharBytesNoexcept(ch);
	if (count == 0u) {
		throw InvalidUtf8InitialByte(ch);
	}
	return count;
}


/**
 * Count UTF8 characters in a sequence.
 *
 * @param begin an iterator to the beginning of the sequence.
 */
template <typename InputIter>
unsigned
countUtf8Chars(InputIter begin, InputIter end) {
	unsigned count = 0u;
	while (begin != end) {
		std::advance(begin, countNextUtf8CharBytes(*begin));
		++count;
	}
	return count;
}


/**
 * Count UTF8 characters in a null-terminated sequence.
 *
 * @param begin an iterator to the beginning of the sequence.
 */
template <typename InputIter>
unsigned
countUtf8Chars(InputIter begin) {
	unsigned count = 0u;
	char ch = *begin;
	while (ch != '\0') {
		std::advance(begin, countNextUtf8CharBytes(ch));
		ch = *begin;
		++count;
	}
	return count;
}


/**
 * Copyright (C) 2015 Florian Kesseler
 *
 * This function is an alteration of `utf8_scan` implemented by Florian
 * Kesseler for the Motor2d library, distributed under the MIT license. See
 * @ref licenses/LICENSE.Motor2d.md for details.
 *
 * See https://github.com/rnlf/motor/blob/master/src/tools/utf8.c for the
 * original implementation.
 *
 * @returns a pair containing the codepoint and the number of bytes until the
 * next character.
 */
template <typename InputIter>
constexpr std::pair<unicode_codepoint, int>
peekNextUtf8Char(InputIter iBytes) {
	std::uint8_t const u = *reinterpret_cast<std::uint8_t const*>(&(*iBytes));
	unicode_codepoint codepoint = 0u;
	unsigned more = 0u;
	if (u < 0x80) {
		codepoint = u & 0x7F;
	}
	else if ((u & 0xE0) == 0xC0) {
		codepoint = u & 0x1F;
		more = 1;
	}
	else if ((u & 0xF0) == 0xE0) {
		codepoint = u & 0x0F;
		more = 2;
	}
	else if ((u & 0xF8) == 0xF0) {
		codepoint = u & 0x07;
		more = 3;
	}
	else {
		throw InvalidUtf8InitialByte(*iBytes);
	}

	for(unsigned i = 0u; i < more; ++i) {
		codepoint = (codepoint << 6) | (*(++iBytes) & 0x3F);
	}

	return {codepoint, more + 1};
}


template <typename InputIter>
inline unicode_codepoint
readNextUtf8Char(InputIter& it) {
	auto const result = peekNextUtf8Char(it);
	std::advance(it, result.second);
	return result.first;
}


/**
 * UTF8 iterator adaptor.
 *
 * Adapts an input iterator over a byte sequence to a UTF8 codepoint iterator.
 */
template <typename InputIter>
class Utf8Iterator {
public:
	using base_iterator = InputIter;
	using utf8_iterator = Utf8Iterator;
	using difference_type = std::ptrdiff_t;
	using value_type = unicode_codepoint;
	using pointer = value_type*;
	using const_pointer = value_type const*;
	using reference = value_type;
	using const_reference = value_type const&;

	/**
	 * Iterator category.
	 *
	 * Utf8Iterator is an input iterator but not a forward iterator, since we
	 * can't return a reference type.
	 */
	using iterator_category = std::input_iterator_tag;

	/**
	 * Construct a Utf8Iterator.
	 *
	 * Prefer @ref utf8_iterator for constructing Utf8Iterators from arbitrary
	 * input iterator types, as @ref utf8_iterator returns the original
	 * iterator when the latter is already a Utf8Iterator instance.
	 */
	constexpr Utf8Iterator(InputIter it)
		:	m_it{it}
	{
	}

	inline Utf8Iterator&
	operator++() {
		auto const numBytes = countNextUtf8CharBytes(*m_it);
		std::advance(m_it, numBytes);
		return *this;
	}

	inline Utf8Iterator
	operator++(int) {
		Utf8Iterator out{*this};
		++(*this);
		return out;
	}

	inline unicode_codepoint
	operator*() const {
		return peekNextUtf8Char(m_it).first;
	}

	inline bool operator==(Utf8Iterator it) const { return m_it == it.m_it; }
	inline bool operator!=(Utf8Iterator it) const { return m_it != it.m_it; }

	inline bool operator>(Utf8Iterator it) const { return m_it > it.m_it; }
	inline bool operator<(Utf8Iterator it) const { return m_it < it.m_it; }

private:
	base_iterator m_it;
};


/** TMP class for determining the UTF8 iterator class for an iterator type. */
template <typename InputIter>
struct Utf8IteratorType {
	using type = Utf8Iterator<InputIter>;
};


/** Specialisation of Utf8IteratorType for Utf8Iterator types. */
template <typename InputIter>
struct Utf8IteratorType<Utf8Iterator<InputIter>> {
	using type = Utf8Iterator<InputIter>;
};


/** TMP alias for @ref Utf8IteratorType. */
template <typename InputIter>
using utf8_iterator_t = typename Utf8IteratorType<InputIter>::type;


/** Construct a Utf8Iterator from an iterator. */
template <typename InputIter>
inline utf8_iterator_t<InputIter>
utf8_iterator(InputIter it) {
	return utf8_iterator_t<InputIter>(it);
}


/**
 * Specialisation for Utf8Iterators.
 *
 * @returns a copy of the original iterator.
 */
template <typename InputIter>
inline Utf8Iterator<InputIter>
utf8_iterator(Utf8Iterator<InputIter> it) {
	return it;
}


} // namespace glif
#endif // #ifndef GLIF_INCLUDE__GLIF__UNICODE__H

#ifndef GLIF_INCLUDE__GLIF__FREETYPE__INIT__H
#define GLIF_INCLUDE__GLIF__FREETYPE__INIT__H
#include <memory>
#include <cstdint>


struct FT_LibraryRec_;
struct FT_FaceRec_;


namespace glif {
namespace freetype {


std::shared_ptr<FT_LibraryRec_>
init();


std::shared_ptr<FT_FaceRec_>
createFace(FT_LibraryRec_* pLib, char const* pFilePath, long fontIndex);


void
loadGlyphIntoSlot(FT_FaceRec_* pFace, std::uint32_t codepoint);


} // namespace freetype
} // namespace glif
#endif // #ifndef GLIF_INCLUDE__GLIF__FREETYPE__INIT__H

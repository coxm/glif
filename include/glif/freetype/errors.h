#ifndef GLIF_INCLUDE__GLIF__FREETYPE__ERRORS__H
#define GLIF_INCLUDE__GLIF__FREETYPE__ERRORS__H
#include <ft2build.h>
#include FT_FREETYPE_H


namespace glif {
namespace freetype {


struct ErrorInfo {
	const char* m_pMessage;
	int m_code;
	int m_numeric;
};


/** Get additional information for an error code. */
ErrorInfo const&
getErrorInfo(int errorCode) noexcept;


/** Get the error string for a particular error code. */
inline char const*
getErrorString(int errorCode) noexcept {
	return getErrorInfo(errorCode).m_pMessage;
}


/** Ensure no errors occurred. */
void
ensureNoError(int code);


// Set up a table of FreeType error strings.
// See https://www.freetype.org/freetype2/docs/reference/ft2-error_enumerations.html for an explanation.
#undef FTERRORS_H_
#define FT_ERRORDEF(code, numeric, pMessage)  { pMessage, code, numeric },
#define FT_ERROR_START_LIST     {
#define FT_ERROR_END_LIST       { "(Invalid error code)", -1, -1 } };
constexpr ErrorInfo g_errorInfo[] = 
#include FT_ERRORS_H


constexpr std::size_t g_errorInfoCount =
	(sizeof(g_errorInfo) / sizeof(ErrorInfo)) - 1ul;


} // namespace freetype
} // namespace glif
#endif // #ifndef GLIF_INCLUDE__GLIF__FREETYPE__ERRORS__H

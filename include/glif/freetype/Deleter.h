#ifndef GLIF_INCLUDE__GLIF__FREETYPE__DELETER__H
#define GLIF_INCLUDE__GLIF__FREETYPE__DELETER__H
struct FT_LibraryRec_;
struct FT_FaceRec_;


// Note: FT_Error is guaranteed to be a typedef of `int`:
// https://www.freetype.org/freetype2/docs/reference/ft2-basic_types.html#FT_Error
extern "C" int FT_Done_FreeType(FT_LibraryRec_*);
extern "C" int FT_Done_Face(FT_FaceRec_*);


namespace glif {
namespace freetype {


struct Deleter {
	inline void operator()(FT_LibraryRec_* pLib) const noexcept {
		FT_Done_FreeType(pLib);
	}

	inline void operator()(FT_FaceRec_* pFace) const noexcept {
		FT_Done_Face(pFace);
	}
};


} // namespace freetype
} // namespace glif
#endif // #ifndef GLIF_INCLUDE__GLIF__FREETYPE__DELETER__H

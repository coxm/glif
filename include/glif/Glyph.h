#ifndef GLIF_INCLUDE__GLIF__HEADER__H
#define GLIF_INCLUDE__GLIF__HEADER__H
#include <cstdint>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "./Dimensions.h"


struct FT_GlyphSlotRec_;


namespace glif {


struct Glyph {
	Glyph(
		FT_GlyphSlotRec_ const* const pGlyph,
		std::size_t bitmapBegin,
		std::size_t bitmapEnd);

	inline Glyph(
		unsigned glyphWidth,
		unsigned glyphHeight,
		int glyphBearingX,
		int glyphBearingY,
		FT_Vector glyphAdvance,
		std::size_t bitmapBeginIndex,
		std::size_t bitmapEndIndex
	)
		:	width(glyphWidth)
		,	height(glyphHeight)
		,	bearingX(glyphBearingX)
		,	bearingY(glyphBearingY)
		,	advance(glyphAdvance)
		,	begin(bitmapBeginIndex)
		,	end(bitmapEndIndex)
	{
	}

	/** The glyph's width. */
	unsigned width;
	/** The glyph's height. */
	unsigned height;
	/** The glyph's current packing position. */
	int textureX = 0u;
	/** The glyph's current packing y-coordinate. */
	int textureY = 0u;
	/** The x-offset from the baseline to the left of the glyph. */
	int bearingX;
	/** The y-offset from the baseline to the top of the glyph. */
	int bearingY;
	/** The offset to advance to the next glyph. */
	FT_Vector advance;
	/** The begin index for the glyph's bitmap data. */
	std::size_t begin;
	/** The one-past-the-end index for the glyph's bitmap data. */
	std::size_t end;
};


} // namespace glif
#endif // #ifndef GLIF_INCLUDE__GLIF__HEADER__H

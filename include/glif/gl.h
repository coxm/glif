#ifndef GLIF_INCLUDE__GLIF__GL__H
#define GLIF_INCLUDE__GLIF__GL__H
#include "glif/config.h"


#ifdef glif_GLAD
# include <glad/glad.h>
#else
# include <GL/glew.h>
# include <GL/gl.h>
#endif


#endif // #ifndef GLIF_INCLUDE__GLIF__GL__H

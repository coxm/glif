#include "glif/Glyph.h"


namespace glif {


Glyph::Glyph(
	FT_GlyphSlotRec_ const* const pGlyph,
	std::size_t bitmapBegin,
	std::size_t bitmapEnd
)
	:	width(pGlyph->bitmap.width)
	,	height(pGlyph->bitmap.rows)
	,	bearingX(pGlyph->bitmap_left)
	,	bearingY(pGlyph->bitmap_top)
	,	advance(pGlyph->advance)
	,	begin(bitmapBegin)
	,	end(bitmapEnd)
{
}


} // namespace glif

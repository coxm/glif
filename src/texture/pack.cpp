#include "glif/texture/pack.h"


namespace glif {
namespace texture {


std::uint32_t
leastTwoPowerGte(std::uint32_t n) noexcept {
	--n;
	n |= n >> 1u;
	n |= n >> 2u;
	n |= n >> 4u;
	n |= n >> 8u;
	n |= n >> 16u;
	++n;
	return n;
}


GLuint
packGLTexture2D(std::uint8_t const* pTextureData, int width, int height) {
	GLuint texture;
	glGenTextures(1, &texture);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(
		GL_TEXTURE_2D,
		0, // Level.
		GL_RED,
		width,
		height,
		0, // Border (must be zero).
		GL_RED,
		GL_UNSIGNED_BYTE,
		pTextureData);
	return texture;
}


Dimensions
attemptRectPack(
	stbrp_rect* pRectangles,
	int numRectangles,
	std::uint32_t textureSideHint,
	int maxTextureSide,
	Padding const padding
) {
	if (maxTextureSide <= 0) {
		glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSide);
	}

	stbrp_context context;
	for (
		int side = int(leastTwoPowerGte(textureSideHint));
		side <= maxTextureSide;
		side *= 2
	) {
		int const numNodes = (3 * side) / 2;
		auto nodes = std::make_unique<stbrp_node[]>(numNodes);
		stbrp_init_target(&context, side, side, nodes.get(), numNodes);
		if (stbrp_pack_rects(&context, pRectangles, numRectangles) == 1) {
			return {side, side};
		}
	}

	return {-1, -1};
}


} // namespace texture
} // namespace glif

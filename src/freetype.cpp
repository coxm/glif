#include "glif/errors.h"
#include "glif/freetype.h"


namespace glif {
namespace freetype {


ErrorInfo const&
getErrorInfo(int errorCode) noexcept {
	for (unsigned i = 0u; i < g_errorInfoCount; ++i) {
		if (errorCode == g_errorInfo[i].m_code) {
			return g_errorInfo[i];
		}
	}

	// Return the final "Invalid error code" object.
	return g_errorInfo[g_errorInfoCount];
}


/** Ensure no errors occurred. */
void
ensureNoError(int code) {
	if (code) {
		throw FreeTypeError(code);
	}
}


std::shared_ptr<FT_LibraryRec_>
init() {
	FT_Library pLib;
	auto const error = FT_Init_FreeType(&pLib);
	std::shared_ptr<FT_LibraryRec_> pShared(pLib, Deleter());
	ensureNoError(error);
	return pShared;
}


std::shared_ptr<FT_FaceRec_>
createFace(FT_Library pLib, char const* pFilePath, long fontIndex) {
	FT_Face pFace;
	auto const error = FT_New_Face(pLib, pFilePath, fontIndex, &pFace);
	std::shared_ptr<FT_FaceRec_> pUnique(pFace, Deleter());
	ensureNoError(error);
	return pUnique;
}


void
loadGlyphIntoSlot(FT_Face pFace, std::uint32_t codepoint) {
	auto const errorCode = FT_Load_Char(pFace, codepoint, FT_LOAD_RENDER);
	if (errorCode != 0) {
		throw FreeTypeError(errorCode);
	}
}


} // namespace freetype
} // namespace glif

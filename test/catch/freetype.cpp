#include "catch.hpp"

#include "glif/freetype.h"


TEST_CASE("freetype::init", "[freetype]") {
	SECTION("returns a shared_ptr to the FT_Library") {
		auto const pFreeType = glif::freetype::init();
		CHECK(bool(pFreeType));
	}
	SECTION("returns a unique_ptr which can be safely reset") {
		// (memcheck)
		auto pFreeType = glif::freetype::init();
		pFreeType.reset();
		CHECK(true);
	}
}


TEST_CASE("freetype::createFace", "[freetype]") {
	auto const pFreeType = glif::freetype::init();

	SECTION("returns a unique_ptr to the face") {
		auto const pFace = glif::freetype::createFace(
			pFreeType.get(), TEST_FONT_PATH, TEST_FONT_INDEX);
		CHECK(bool(pFace));
	}
	SECTION("returns a unique_ptr which can be safely reset") {
		// (memcheck)
		auto const pFace = glif::freetype::createFace(
			pFreeType.get(), TEST_FONT_PATH, TEST_FONT_INDEX);
		CHECK(true);
	}
}

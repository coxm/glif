#include <cstring>

#include "catch.hpp"

#include "glif/unicode.h"
#include "glif/errors.h"


TEST_CASE("countNextUtf8CharBytes", "[unicode]") {
	SECTION("returns 1 for 7-bit ASCII characters") {
		for (int i = 0; i <= 127; ++i) { // Use an int to prevent overflow.
			auto const count = glif::countNextUtf8CharBytes(char(i));
			CHECK(count == 1u);
		}
	}
	SECTION("returns 1 for 7-bit ASCII character pointers") {
		for (int i = 0; i <= 127; ++i) { // Use an int to prevent overflow.
			auto const c = char(i);
			auto const count = glif::countNextUtf8CharBytes(&c);
			CHECK(count == 1u);
		}
	}
	SECTION("returns the number of bytes for non-7-bit characters ($)") {
		constexpr char const* pUtf8 = "$";
		CHECK(glif::countNextUtf8CharBytes(pUtf8) == 1u);
	}
	SECTION("returns the number of bytes for non-7-bit characters (¢)") {
		constexpr char const* pUtf8 = "¢";
		CHECK(glif::countNextUtf8CharBytes(pUtf8) == 2u);
	}
	SECTION("returns the number of bytes for non-7-bit characters (€)") {
		constexpr char const* pUtf8 = "€";
		CHECK(glif::countNextUtf8CharBytes(pUtf8) == 3u);
	}
	SECTION("returns the number of bytes for non-7-bit characters (𐍈)") {
		constexpr char const* pUtf8 = "𐍈";
		CHECK(glif::countNextUtf8CharBytes(pUtf8) == 4u);
	}
	SECTION("throws InvalidUtf8InitialByte for invalid bytes") {
		constexpr char const* pUtf8 = "𐍈";
		CHECK_THROWS_AS(
			glif::countNextUtf8CharBytes(pUtf8 + 1),
			glif::InvalidUtf8InitialByte);
	}
}


TEST_CASE("countNextUtf8CharBytesNoexcept", "[unicode]") {
	SECTION("returns 1 for 7-bit ASCII characters") {
		for (int i = 0; i <= 127; ++i) { // Use an int in case of overflow.
			char c(i);
			CHECK(glif::countNextUtf8CharBytesNoexcept(c) == 1u);
		}
	}
	SECTION("returns the number of bytes for non-7-bit characters ($)") {
		constexpr char const* pUtf8 = "$";
		CHECK(glif::countNextUtf8CharBytesNoexcept(pUtf8[0]) == 1u);
	}
	SECTION("returns the number of bytes for non-7-bit characters (¢)") {
		constexpr char const* pUtf8 = "¢";
		CHECK(glif::countNextUtf8CharBytesNoexcept(pUtf8[0]) == 2u);
	}
	SECTION("returns the number of bytes for non-7-bit characters (€)") {
		constexpr char const* pUtf8 = "€";
		CHECK(glif::countNextUtf8CharBytesNoexcept(pUtf8[0]) == 3u);
	}
	SECTION("returns the number of bytes for non-7-bit characters (𐍈)") {
		constexpr char const* pUtf8 = "𐍈";
		CHECK(glif::countNextUtf8CharBytesNoexcept(pUtf8[0]) == 4u);
	}
	SECTION("returns 0 for continuation bytes") {
		constexpr char const* pUtf8 = "𐍈";
		CHECK(glif::countNextUtf8CharBytesNoexcept(pUtf8[1]) == 0u);
	}
}


TEST_CASE("countUtf8Chars (single C-string argument)", "[unicode]") {
	SECTION("example: \"$\"") { CHECK(glif::countUtf8Chars("$") == 1u); }
	SECTION("example: \"¢\"") { CHECK(glif::countUtf8Chars("¢") == 1u); }
	SECTION("example: \"€\"") { CHECK(glif::countUtf8Chars("€") == 1u); }
	SECTION("example: \"𐍈\"") { CHECK(glif::countUtf8Chars("𐍈") == 1u); }
	SECTION("example: \"ἐξετάζω\"") {
		CHECK(glif::countUtf8Chars("ἐξετάζω") == 7u);
	}
}


TEST_CASE("countUtf8Chars (range iterators)", "[unicode]") {
	SECTION("example: \"$\"") {
		char const* const pInput = "$junk";
		CHECK(glif::countUtf8Chars(pInput, pInput + 1) == 1u);
	}
	SECTION("example: \"¢\"") {
		char const* const pInput = "¢junk";
		CHECK(glif::countUtf8Chars(pInput, pInput + 2) == 1u);
	}
	SECTION("example: \"€\"") {
		char const* const pInput = "€junk";
		CHECK(glif::countUtf8Chars(pInput, pInput + 3) == 1u);
	}
	SECTION("example: \"𐍈\"") {
		char const* const pInput = "𐍈junk";
		CHECK(glif::countUtf8Chars(pInput, pInput + 4) == 1u);
	}
	SECTION("example: \"ἐξετάζω\"") {
		char const* const pInput = "ἐξετάζωjunk";
		CHECK(glif::countUtf8Chars(pInput, pInput + 15) == 7u);
	}
}


TEST_CASE("peekNextUtf8Char", "[unicode]") {
	std::string bytes{"ἐξετάζω"};
	auto const result = glif::peekNextUtf8Char(bytes.begin());

	SECTION("returns the decoded unicode value for a byte string") {
		CHECK(result.first == 7952);
	}
	SECTION("returns the number of bytes until the next byte string") {
		char const* const pNext = bytes.data() + result.second;
		CHECK(std::strcmp(pNext, "ξετάζω") == 0);
	}
}


TEST_CASE("readNextUtf8Char", "[unicode]") {
	char const* pBytes = "ἐξετάζω";

	SECTION("returns the decoded unicode value for a byte string") {
		CHECK(glif::readNextUtf8Char(pBytes) == 7952);
	}
	SECTION("advances the pointer to the next unicode character") {
		glif::readNextUtf8Char(pBytes);
		CHECK(std::strcmp(pBytes, "ξετάζω") == 0);
	}
}
